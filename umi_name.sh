#/usr/bin/bash
#$ -cwd
#$ -l h_vmem=4G
#$ -o logs/umi_name/
#$ -e logs/umi_name/

. /etc/profile.d/modules.sh

module load anaconda/5.3.1
source activate dedup

for file in data/K562-TIA1*.gz
do
    echo processing $file
    python update_name.py $file umi_name${file##data}
done
