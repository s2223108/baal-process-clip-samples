import gzip
from argparse import ArgumentParser
from pathlib import Path

from pysam import FastxFile

def parse_args():
    parser = ArgumentParser(description = ('Utility to copy UMI from start of name field to the end.' 
                                           'Intended to be used to enable further processing by UMI tools'))

    parser.add_argument('infile', help='Input fastq file', type=Path)
    parser.add_argument('outfile', help='output fastq file', type=Path)
    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()

    fasta = FastxFile(args.infile)

    # Open as compressed if output file ends with .gz'
    if args.outfile.suffix == '.gz':
       fopen = lambda file: gzip.open(file, 'wt')
    else:
       fopen = lambda file: open(file, 'w')

    with fopen(args.outfile) as outfile:
        for entry in fasta:
            umi = entry.name.split(':')[0]
            entry.name = f"{entry.name}_{umi}"
            outfile.write(f"{entry}\n")

